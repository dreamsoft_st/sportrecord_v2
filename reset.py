from unipath import Path
from subprocess import call
import os

this_file = Path(__file__).absolute()
current_dir = this_file.parent
dir_list = current_dir.listdir()

for paths in dir_list:
	migration_folder = paths.child('migrations')
	if migration_folder.exists():
		list_files = migration_folder.listdir()
		for files in list_files:
			split = files.components()
			if split[-1] != Path('__init__.py'):
				files.remove()

#if os.path.exists('db.sqlite3'):
#	os.remove('db.sqlite3')
#call('manage.py makemigrations', shell=True)
#call('manage.py migrate', shell=True)
#call('manage.py syncdb', shell=True)