# -*- coding: utf-8 -*-
from django.db import models

class Faq(models.Model):
	pregunta = models.CharField(max_length=200) 
	respuesta = models.TextField()

	def __str__(self):
		return self.pregunta

class Contacto(models.Model):
	direccion = models.CharField(max_length=300)
	telefono = models.CharField(max_length=12)
	celular = models.CharField(max_length=12)
	email = models.EmailField(blank=True)

	def __unicode__(self):
		return self.telefono

	def validar_contacto(self):
		if Contacto.objects.all().count() > 0:
			if self.pk is None:
				return False
		return True

	def clean(self):
		if not self.validar_contacto():
			raise ValidationError('Solo se permite un contacto, porfavor modifique el existente')

class Nosotros(models.Model):
	mensaje = models.TextField()

	def __unicode__(self):
		return self.mensaje

	def validar_nosotros(self):
		if Nosotros.objects.all().count() > 0:
			if self.pk is None:
				return False
		return True

	def clean(self):
		if not self.validar_nosotros():
			raise ValidationError('Solo se permite un nosotros, porfavor modifique el existente')

class Servicios(models.Model):
	mensaje = models.TextField()

	def __unicode__(self):
		return self.mensaje

	def validar_servicios(self):
		if Servicios.objects.all().count() > 0:
			if self.pk is None:
				return False
		return True

	def clean(self):
		if not self.validar_servicios():
			raise ValidationError('Solo se permite un servicio, porfavor modifique el existente')