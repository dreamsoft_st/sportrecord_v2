from django.shortcuts import render
from .models import Nosotros, Servicios

def nosotros(request):
	m = Nosotros.objects.all().first()
	mensaje = ''
	if m is not None:
		mensaje = m.mensaje
	data = {
		'mensaje_info': mensaje,
		'titulo': 'Nosotros',
	}
	return render(request, 'info.html', data)

def servicios(request):
	m = Servicios.objects.all().first()
	mensaje = ''
	if m is not None:
		mensaje = m.mensaje
	data = {
		'mensaje_info': mensaje,
		'titulo': 'Servicios',
	}
	return render(request, 'info.html', data)