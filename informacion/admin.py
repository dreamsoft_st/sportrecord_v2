# -*- coding: utf-8 -*-
from django import forms
from django.db import models
from django.contrib import admin
from .models import Faq, Contacto, Nosotros, Servicios

@admin.register(Faq)
class FaqAdmin(admin.ModelAdmin):
	list_display = ('pregunta', 'Respuesta',)
	search_fields = ('pregunta',)
	list_display_links = ('pregunta',)

	def Respuesta(self, obj):
		return obj.respuesta
	Respuesta.allow_tags = True

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)


@admin.register(Contacto)
class ContactoAdmin(admin.ModelAdmin):
	list_display = ('telefono', 'celular', 'email',)
	list_display_links = ('telefono', 'celular', 'email',)

@admin.register(Nosotros, Servicios)
class NosotrosAdmin(admin.ModelAdmin):
	list_display = ('pk', 'Mensaje',)

	def Mensaje(self, obj):
		return obj.mensaje[:20]