# -*- coding: utf-8 -*-s
from django.shortcuts import render, get_object_or_404, redirect
from django.http import HttpResponse
from django.core.servers.basehttp import FileWrapper
from django.contrib.auth.decorators import login_required
from .models import Foto, Categoria, Evento
from django.db.models import Q
import datetime, calendar, mimetypes, os

from pago.models import Compra
from django.utils.encoding import smart_str


# Create your views here.
def categoria(request, categoria):
	categorias = get_object_or_404(Categoria, slug=categoria)
	eventos = Evento.objects.filter(categoria=categorias)

	return render(request, 'lista_evento.html', {'categoria':categorias, 'eventos': eventos})

def eventos(request):
	eventos = Evento.objects.all()
	return render(request, 'lista_evento.html', {'eventos': eventos})

def evento(request, evento):
	evt = get_object_or_404(Evento, slug=evento)
	fotos = Foto.objects.filter(evento=evt.pk)
	return render(request, 'evento.html', {'titulo': ('Evento ' + evt.nombre), 'fotos': fotos, 'evento': evento })

def buscarevento_by_cat(request, slug):
	categoria = get_object_or_404(Categoria, slug=slug)
	nombre = request.GET.get('dateve', '')
	ciudad = request.GET.get('ciudeve', '')
	fecha = request.GET.get('fecheve', '')
	
	q = Q()
	if len(nombre) > 0:
		q.add(Q(nombre__icontains=nombre), Q.OR)

	if len(ciudad) > 0:
		ciudad = ciudad.split(',')[0]
		q.add(Q(ciudad__nombre=ciudad), Q.OR)

	if len(fecha) > 0:
		mes = int(fecha.split('-')[1])
		ano = int(fecha.split('-')[0])
		d = calendar.monthrange(ano, mes)
		dia_min = int(d[0])
		dia_max = int(d[1])
		q.add((Q(fecha__gt=datetime.date(ano, mes, dia_min)) & Q(fecha__lt=datetime.date(ano, mes, dia_max))), Q.OR)

	q.add(Q(categoria=categoria.pk), Q.AND)
	eventos_list = Evento.objects.filter(q)
	return render(request, 'lista_evento.html', {'eventos':eventos_list, 'categoria':categoria})

def buscarevento(request):
	nombre = request.GET.get('dateve', '')
	ciudad = request.GET.get('ciudeve', '')
	fecha = request.GET.get('fecheve', '')
	
	q = Q()
	if len(nombre) > 0:
		q.add(Q(nombre__icontains=nombre), Q.OR)

	if len(ciudad) > 0:
		ciudad = ciudad.split(',')[0]
		q.add(Q(ciudad__nombre=ciudad), Q.OR)

	if len(fecha) > 0:
		mes = int(fecha.split('-')[1])
		ano = int(fecha.split('-')[0])
		d = calendar.monthrange(ano, mes)
		dia_min = int(d[0])
		dia_max = int(d[1])
		q.add((Q(fecha__gt=datetime.date(ano, mes, dia_min)) & Q(fecha__lt=datetime.date(ano, mes, dia_max))), Q.OR)

	eventos_list = Evento.objects.filter(q)
	return render(request, 'lista_evento.html', {'eventos':eventos_list, 'categoria':categoria})


@login_required
def misfotos(request):
	compras = Compra.objects.filter(usuario=request.user, pagado=True)
	return render(request, 'fotosadquiridas.html', {'compras' : compras})

@login_required
def pedidos(request):
	pedidos = Compra.objects.filter(usuario=request.user)
	return render(request, 'pedidos.html', {'pedidos' : pedidos})

@login_required
def descargar(request):
	foto_id = request.GET.get('foto_id', None)
	if foto_id is not None:
		foto_id = int(foto_id)
		f = Foto.objects.filter(pk=foto_id).first()
		if Compra.objects.filter(usuario=request.user, foto=f).exists():
			path = f.download_url
			nombre = smart_str( os.path.basename( path ) )
			ext = nombre.split('.')[0]

			nombre = smart_str( nombre.replace(ext, f.slug) )

			wrapper = FileWrapper( open( path, "rb" ) )
			content_type = mimetypes.guess_type( path )[0]
			response = HttpResponse(wrapper, content_type = content_type)
			response['Content-Length'] = os.path.getsize( path )
			response['Content-Disposition'] = 'attachment; filename=%s' % ( nombre )
			return response