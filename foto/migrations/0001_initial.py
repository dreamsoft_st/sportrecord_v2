# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import djmoney.models.fields
from decimal import Decimal


class Migration(migrations.Migration):

    dependencies = [
        ('estado', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Categoria',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=50)),
                ('imagen', models.ImageField(upload_to=b'categorias')),
                ('slug', models.SlugField(max_length=60, editable=False, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Evento',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=40)),
                ('descripcion', models.TextField()),
                ('fecha', models.DateField()),
                ('slug', models.SlugField(editable=False, blank=True)),
                ('categoria', models.ForeignKey(to='foto.Categoria')),
                ('ciudad', models.ForeignKey(to='estado.Ciudad')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Folio',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('folio', models.PositiveIntegerField(default=0)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Foto',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('folio', models.CharField(max_length=50, blank=True)),
                ('nombre', models.CharField(max_length=100)),
                ('descripcion', models.TextField(blank=True)),
                ('imagen', models.ImageField(upload_to=b'fotos')),
                ('precio_currency', djmoney.models.fields.CurrencyField(default=b'MX', max_length=3, editable=False, choices=[(b'MX', b'Peso mexicano')])),
                ('precio', djmoney.models.fields.MoneyField(default=Decimal('0.0'), max_digits=10, decimal_places=2, default_currency=b'MX')),
                ('votos', models.PositiveIntegerField(default=0, editable=False)),
                ('vendido', models.PositiveIntegerField(default=0, editable=False)),
                ('ancho', models.PositiveIntegerField(default=0, editable=False)),
                ('alto', models.PositiveIntegerField(default=0, editable=False)),
                ('thumbnail', models.ImageField(upload_to=b'fotos', editable=False, blank=True)),
                ('slug', models.SlugField(max_length=150, editable=False, blank=True)),
                ('download_url', models.CharField(max_length=200, editable=False, blank=True)),
                ('evento', models.ForeignKey(to='foto.Evento')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Persona',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('nombre', models.CharField(max_length=100)),
                ('folios', models.ManyToManyField(to='foto.Folio', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='foto',
            name='personas',
            field=models.ManyToManyField(to='foto.Persona', null=True, blank=True),
            preserve_default=True,
        ),
    ]
