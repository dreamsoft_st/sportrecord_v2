# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('foto', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='foto',
            name='folio',
        ),
        migrations.AlterField(
            model_name='evento',
            name='nombre',
            field=models.CharField(unique=True, max_length=50),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='evento',
            name='slug',
            field=models.SlugField(max_length=100, editable=False, blank=True),
            preserve_default=True,
        ),
    ]
