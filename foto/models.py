# -*- coding: utf-8 -*-
from django.db import models
from djmoney.models.fields import MoneyField
from django.conf import settings
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save

from estado.models import Ciudad
from .images import watermark

try:
	from PIL import Image
except ImportException:
	import Image 
import os, shutil


class Categoria(models.Model):
	nombre = models.CharField(max_length=50)
	imagen = models.ImageField(upload_to='categorias')
	slug = models.SlugField(max_length=60, blank=True, editable=False)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)		
		super(Categoria, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.nombre

class Evento(models.Model):
	nombre = models.CharField(max_length=50, unique=True)
	descripcion = models.TextField()
	fecha = models.DateField()
	ciudad = models.ForeignKey(Ciudad)
	categoria = models.ForeignKey(Categoria)
	slug = models.SlugField(max_length=100, blank=True, editable=False)

	def save(self, *args, **kwargs):
		self.slug = slugify(self.nombre)		
		super(Evento, self).save(*args, **kwargs)

	def __unicode__(self):
		return self.nombre

class Folio(models.Model):
	folio = models.PositiveIntegerField(default=0)

	def __unicode__(self):
		return unicode(self.folio)

class Persona(models.Model):
	nombre = models.CharField(max_length=100)
	folios = models.ManyToManyField(Folio, null=True, blank=True)

	def __unicode__(self):
		return self.nombre

class Foto(models.Model):
	nombre = models.CharField(max_length=100)
	descripcion = models.TextField(blank=True)
	imagen = models.ImageField(upload_to='fotos')
	precio = MoneyField(max_digits=10, decimal_places=2, default_currency='MX')
	evento = models.ForeignKey(Evento)
	votos = models.PositiveIntegerField(default=0, editable=False)
	vendido = models.PositiveIntegerField(default=0, editable=False)
	ancho = models.PositiveIntegerField(default=0, editable=False)
	alto = models.PositiveIntegerField(default=0, editable=False)
	thumbnail = models.ImageField(upload_to='fotos', editable=False, blank=True)
	slug = models.SlugField(max_length=150, editable=False, blank=True)
	download_url = models.CharField(max_length=200, editable=False, blank=True)
	personas = models.ManyToManyField(Persona, null=True, blank=True)

	def __str__(self):
		return self.nombre

	def save(self, *args, **kwards):
		self.slug = slugify(self.nombre)
		super(Foto, self).save(*args, **kwards)


def change_image(sender, instance, **kwargs):
	if len(instance.imagen.name) == 0 or "thumbnail" in instance.imagen.name:
		return True

	ext = instance.imagen.name.split('.')[-1]
	filename = 'fotos/{}.{}'.format(instance.pk, ext)
	new_filename = 'fotos/{}_thumbnail.png'.format(instance.pk)
	direccion = os.path.join(settings.MEDIA_ROOT, instance.imagen.name)
	dir_file = os.path.join(settings.MEDIA_ROOT, filename)
	direccion_imagenes = os.path.join(settings.MEDIA_ROOT_IMG, filename)
	new_file = dir_file.replace(filename, 'fotos/{}_thumbnail.png'.format(instance.pk))

#	direccion_imagenes = direccion_imagenes.replace('fotos/', '')

	if filename != instance.imagen.name and os.path.exists(direccion):
		if os.path.exists(dir_file):
			os.remove(dir_file)
		os.rename(direccion, dir_file)
		direccion = dir_file

	if os.path.exists(direccion):
		
		Foto.objects.filter(pk=instance.pk).update(thumbnail=new_filename, imagen=new_filename, download_url=direccion_imagenes)
		if os.path.exists(new_file):
			os.remove(new_file)

		if settings.STATIC_ROOT != None:
			logo = os.path.join(settings.STATIC_ROOT, 'img/logo.png')
		else:
			logo = os.path.join(settings.BASE_DIR, 'static/img/logo.png')

		baseim = Image.open(direccion)
		datos = baseim.size
		Foto.objects.filter(pk=instance.pk).update(ancho=int(datos[0]), alto=int(datos[1]))
		size = (600, 600)
		baseim.thumbnail(size, Image.ANTIALIAS)
		background = Image.new('RGBA', size, (255, 255, 255, 0))
		background.paste(
		    baseim,
		    ((size[0] - baseim.size[0]) / 2, (size[1] - baseim.size[1]) / 2))
		logoim = Image.open(logo)
		watermark(background, logoim, position='C', opacity=0.8, scale='f', rotation=0).save(new_file, 'PNG')		

		if os.path.exists(direccion_imagenes):
			os.remove(direccion_imagenes)

		if os.path.exists(direccion):
			shutil.copyfile(direccion, direccion_imagenes)
			#os.rename(direccion, direccion_imagenes)

		if os.path.exists(direccion):
			os.remove(direccion)

post_save.connect(change_image, sender=Foto)