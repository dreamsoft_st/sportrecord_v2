from django import forms
from django.db import models
from django.contrib import admin
from .models import Evento, Foto, Categoria, Persona, Folio

@admin.register(Categoria)
class CategoriaAdmin(admin.ModelAdmin):
	list_display = ('Imagen', 'nombre',)

	def Imagen(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	Imagen.allow_tags = True

@admin.register(Evento)
class EventoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'categoria',)
	raw_id_fields = ('ciudad', 'categoria',)
	
	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

@admin.register(Foto)
class FotoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'Imagen', 'precio', 'evento')
	search_fields = ('nombre',)
	raw_id_fields = ('evento',)
	filter_horizontal = ['personas']

	formfield_overrides = { models.TextField: {'widget': forms.Textarea(attrs={'class':'ckeditor'})}, }
	class Media:
		js = ('ckeditor/ckeditor.js',)

	def Imagen(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	Imagen.allow_tags = True

@admin.register(Persona)
class PersonaAdmin(admin.ModelAdmin):
	list_display = ('nombre',)
	filter_horizontal = ['folios']

@admin.register(Folio)
class FolioAdmin(admin.ModelAdmin):
	list_display = ('folio',)
