from carrito.carrito import Cart

def informacion(request):
	cart = Cart(request.session)
	datos = {
		'Carrito': cart
	}
	if request.session.get('temp_data', False):
		datos['mensaje'] = request.session['temp_data']
		del request.session['temp_data']
	return datos