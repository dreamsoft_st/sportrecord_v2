from django.contrib.auth.models import check_password
from usuario.models import Usuario

class EmailAuthBackend(object):
	def authenticate(self, username=None, password=None):
		try:
			user = Usuario.objects.get(email=username)
			if user.check_password(password):
				return user
		except Usuario.DoesNotExist:
			try:
				user = Usuario.objects.get(username=username)
				if user.check_password(password):
					return user
			except Usuario.DoesNotExist:
				return None
		return None

	def get_user(self, user_id):
		try:
			return Usuario.objects.get(pk=user_id)
		except Usuario.DoesNotExist:
			return None
		return None