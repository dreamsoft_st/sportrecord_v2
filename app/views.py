# -*- coding: utf-8 -*-
from django.shortcuts import render, redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth import authenticate, login, logout
from django.db.models import Q

from foto.models import Foto, Evento, Categoria
from publicidad.models import Banner
from informacion.models import Faq
from usuario.models import Usuario

def home(request):
	eventos = Evento.objects.order_by('nombre').all()
	categorias = Categoria.objects.order_by('nombre').all()
	faqs = Faq.objects.order_by('pregunta').all()
	banners = Banner.objects.filter(estado=True).order_by('?')[:3]

	data = {
		'eventos': eventos,
		'categorias': categorias,
		'faqs': faqs,
		'banners': banners,
		'mensaje': False
	}

	if request.session.get('temp_data', False):
		data['mensaje'] = request.session['temp_data']
		del request.session['temp_data']

	return render(request, 'index.html', data)

def registro(request):
	if request.user.is_authenticated():
		return redirect('/')
	data = {}
	if request.session.get('temp_data', False):
		data['mensaje'] = request.session['temp_data']
		del request.session['temp_data']


	if request.method == 'POST':
		nombre = request.POST['nombre']
		apellido = request.POST['apellido']
		email = request.POST['email']
		password = request.POST['password']
		password2 = request.POST['password2']
		telefono = request.POST['telefono']
		
		if len(email.split('@')) < 0:
			request.session['temp_data'] = 'Ingrese un correo valido'			
		elif len(password) < 5:
			request.session['temp_data'] = 'La contraseña no debe ser menor de 5 caracteres'
		elif password != password2:
			request.session['temp_data'] = 'Las contraseñas no coinciden'
		elif len(nombre) == 0:
			request.session['temp_data'] = 'El nombre no debe estar vacio'
		elif len(apellido) == 0:
			request.session['temp_data'] = 'El apellido no debe estar vacio'
		elif len(telefono) != 0 and telefono.isdigit() == False:
			request.session['temp_data'] = 'El telefono debe ser solo numeros'
		
		if Usuario.objects.filter(email=email).exists() or Usuario.objects.filter(username=email).exists():
			request.session['temp_data'] = 'Ya existe un usuario registrado con ese email'

		if request.session.get('temp_data', False):
			return render(request, 'registro.html', data)

		user = Usuario()
		user.username = email
		user.email = email
		user.first_name = nombre
		user.last_name = apellido
		user.telefono = telefono
		user.set_password(password)
		user.save()
		request.session['temp_data'] = 'Registro Exitoso!<br />Antes de continuar es necesario verificar tu correo de Email'
		user = authenticate(username=email, password=password)
		login(request, user)
		return redirect('/')

	return render(request, 'registro.html', data)

def login_view(request):
	if request.method == 'POST':
		email = request.POST['email']
		password = request.POST['password']
		
		user = authenticate( username=email, password=password )
		if user is not None:
			login(request, user)
		else:			
			request.session['temp_data'] = 'Usuario/Contraseña incorrecta'
	return redirect('/')

def logout_view(request):
	logout(request)
	return redirect('/')

def buscar(request):
	nombre = request.GET.get('nombre', None)
	folio = request.GET.get('folio', None)
	evento = request.GET.get('evento', None)
	page = request.GET.get('page', 1)

	filtro = Q()
	filtro_s = Q()
	if nombre is not None:
		filtro.add(Q(nombre__icontains=nombre), Q.OR)
		filtro.add(Q(personas__nombre__icontains=nombre), Q.OR)

		if folio is not None and folio.isdigit():
			filtro_s.add(Q(personas__folios__folio__exact=folio), Q.AND)

		if evento is not None and evento.isdigit():
			filtro_s.add(Q(evento=evento), Q.AND)
		filtro.add(filtro_s, Q.AND)
	else:
		if evento is not None and evento.isdigit():
			filtro.add(Q(evento__slug=evento), Q.OR)




#	if not folio.isdigit():
#		folio = None

#	fotos_list = Foto.objects.filter(
#		Q(nombre__icontains=nombre) | Q(folio=folio) | Q(evento=evento)
#	)
	
	fotos_list = Foto.objects.filter( filtro )

	paginator = Paginator(fotos_list, 8)

	try:
		fotos = paginator.page(page)
	except PageNotAnInteger:
		fotos = paginator.page(1)
	except EmptyPage:
		fotos = paginator.page(paginator.num_pages)

	tit = ''
	if nombre is not None and len(nombre) > 0:
		tit = str(nombre) + ' '
	if folio is not None:
		tit = tit + str(folio) + ' '
	if evento is not None:
		tit = tit + str(evento) + ' '

	data = {
		'fotos': fotos,
		'nombre': nombre,
		'folio': folio,
		'evento': evento,
		'titulo': tit
	}
	return render(request, 'buscar.html', data)

def categorias(request):
	categorias = Categoria.objects.order_by('nombre').all()
	return render(request, 'categorias.html', { 'categorias': categorias })

def mod_datos(request):
	if request.user.is_authenticated():
		data = {}
		if request.session.get('temp_data', False):
			data['mensaje'] = request.session['temp_data']
			del request.session['temp_data']


		if request.method == 'POST':
			nombre = request.POST['nombre']
			apellido = request.POST['apellido']
			password = request.POST['password']
			password2 = request.POST['password2']
			telefono = request.POST['telefono']

			if len(password) < 5:
				request.session['temp_data'] = 'La contraseña no debe ser menor de 5 caracteres'
			elif password != password2:
				request.session['temp_data'] = 'Las contraseñas no coinciden'
			elif len(nombre) == 0:
				request.session['temp_data'] = 'El nombre no debe estar vacio'
			elif len(apellido) == 0:
				request.session['temp_data'] = 'El apellido no debe estar vacio'
			elif len(telefono) != 0 and telefono.isdigit() == False:
				request.session['temp_data'] = 'El telefono debe ser solo numeros'

			if request.session.get('temp_data', False):
				return render(request, 'moddatos.html', data)

			user = Usuario.objects.get(pk=request.user.id)
			user.first_name = nombre
			user.last_name = apellido
			user.telefono = telefono
			user.set_password(password)
			user.save()
			request.session['temp_data'] = 'Cambio Exitoso!'
			logout(request)
			user = authenticate(username=user.email, password=password)
			login(request, user)
			return redirect('/')

		return render(request, 'moddatos.html')

	else:
		return redirect('/')