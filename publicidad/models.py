from django.db import models

# Create your models here.

class Banner(models.Model):
	nombre = models.CharField(max_length=50)
	url = models.CharField(max_length=150, null=True)
	nombre_contacto = models.CharField(max_length=50)
	telefono_contacto = models.CharField(max_length=50)
	fecha_inicio = models.DateField()
	fecha_final = models.DateField(null=True)
	imagen = models.ImageField(upload_to='publicidad')
	estado = models.BooleanField(default=True)

	def __unicode__(self):
		return self.nombre

