from django.db import models
from django.contrib import admin
from .models import Banner

# Register your models here.
@admin.register(Banner)
class BannerAdmin(admin.ModelAdmin):
	list_display = ('Imagen', 'nombre','estado',)

	def Imagen(self, obj):
		return '<img src="/media/%s" width="100">' % obj.imagen
	Imagen.allow_tags = True