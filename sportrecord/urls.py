# -*- coding: utf-8 -*-
from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.conf import settings

from django.views.decorators.csrf import csrf_exempt
from pago.views import MyEndPoint

urlpatterns = patterns('',

	url(r'^$', 'app.views.home', name='home'),
	url(r'^login/$', 'app.views.login_view', name='login'),
	url(r'^logout/$', 'app.views.logout_view', name='logout'),
	url(r'^registro/$', 'app.views.registro', name='registro'),
	
	url(r'^buscar/', 'app.views.buscar', name='buscar'),
	
	url(r'^categorias/$', 'app.views.categorias', name='categorias'),
	url(r'^categoria/([-\w]+)/$', 'foto.views.categoria', name='por_categoria'),
	url(r'^categoria/([-\w]+)/q', 'foto.views.buscarevento_by_cat', name="buscarevento_by_cat"),
	
	url(r'^eventos/$', 'foto.views.eventos', name='eventos'),
	url(r'^evento/([-\w]+)/$', 'foto.views.evento', name='evento'),
	url(r'^eventos/q', 'foto.views.buscarevento', name="buscarevento"),

	url(r'^getciudades$', 'estado.views.getciudades', name="getciudades"),


	url(r'^add/$', 'carrito.views.add', name='carrito-add'),
	url(r'^remove/$', 'carrito.views.remove', name='carrito-remove'),

	url(r'^checkout/$', 'pago.views.checkout', name='checkout'),
	url(r'^checkout/pagar/$', 'pago.views.checkout_pagar', name='checkout_pagar'),

	(r'^checkout/paypal/$', csrf_exempt(MyEndPoint())),

	url(r'^mis-fotos/$', 'foto.views.misfotos', name='misfotos'),
	url(r'^pedidos/$', 'foto.views.pedidos', name='pedidos'),
	url(r'^descargar/', 'foto.views.descargar', name='descargar'),

	url(r'^nosotros/$', 'informacion.views.nosotros', name='nosotros'),
	url(r'^servicios/$', 'informacion.views.servicios', name='servicios'),

	url(r'^mi-perfil/$', 'app.views.mod_datos', name='moddatos'),

	url(r'^admin/', include(admin.site.urls)),
)

if settings.DEBUG:
	# static files (images, css, javascript, etc.)
	urlpatterns += patterns('',
		(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
		'document_root': settings.MEDIA_ROOT}))