# -*- coding: utf-8 -*-
"""
Django settings for sportrecord project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '_7+i@+4@9#m35&x!wbj0150&c51jdfggd0*wkg#h#4w9lj@f_)'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
	'suit',

	'django.contrib.admin',
	'django.contrib.auth',
	'django.contrib.contenttypes',
	'django.contrib.sessions',
	'django.contrib.messages',
	'django.contrib.staticfiles',
	
	'wkhtmltopdf',

	'estado',
	'usuario',
	'foto',
	'informacion',
	'carrito',
	'app',
	'publicidad',
	'pago',
)

MIDDLEWARE_CLASSES = (
#	'django.middleware.cache.UpdateCacheMiddleware',

	'django.contrib.sessions.middleware.SessionMiddleware',
	'django.middleware.common.CommonMiddleware',
	'django.middleware.csrf.CsrfViewMiddleware',
	'django.contrib.auth.middleware.AuthenticationMiddleware',
	'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
	'django.contrib.messages.middleware.MessageMiddleware',
	'django.middleware.clickjacking.XFrameOptionsMiddleware',

#	'django.middleware.cache.FetchFromCacheMiddleware',
)
#CACHE_MIDDLEWARE_ANONYMOUS_ONLY = True

ROOT_URLCONF = 'sportrecord.urls'

WSGI_APPLICATION = 'sportrecord.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
	'default': {
		'ENGINE': 'django.db.backends.postgresql_psycopg2',
		'NAME': 'sportrecord_db',
		'USER': 'sportrecord_db',
		'PASSWORD': 'sportrecord_db_password',
		'HOST': 'web351.webfaction.com',
	}
}

# Internationalization
# https://docs.djangoproject.com/en/1.7/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'America/Mazatlan'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
	os.path.join(BASE_DIR, 'static'),
)
TEMPLATE_DIRS = (
	os.path.join(BASE_DIR, 'templates'),
)

if DEBUG == True:
	STATIC_ROOT = '/home/s00rk/webapps/sportrecord_static/'
	MEDIA_ROOT = '/home/s00rk/webapps/sportrecord_media/'
	MEDIA_ROOT_IMG = '/home/s00rk/webapps/sportrecord_images/'
else:
	#STATIC_ROOT = os.path.join(BASE_DIR, 'static')
	MEDIA_ROOT = os.path.join(BASE_DIR, 'media')
	MEDIA_ROOT_IMG = os.path.join(BASE_DIR, 'media')
MEDIA_URL = '/media/'


SUIT_CONFIG = {
	'ADMIN_NAME': 'Sport~Record',
	'HEADER_DATE_FORMAT': 'l, j. F Y',
	'HEADER_TIME_FORMAT': 'h:i a',
	'MENU_EXCLUDE': ('auth.group', 'auth'),
}

from django.conf.global_settings import TEMPLATE_CONTEXT_PROCESSORS as TCP

TEMPLATE_CONTEXT_PROCESSORS = TCP + (
	'django.core.context_processors.request',
	'app.proccesors.informacion',
)


AUTHENTICATION_BACKENDS = ('app.backends.EmailAuthBackend',)
LOGIN_URL = '/registro'

AUTH_USER_MODEL = 'usuario.Usuario'

import moneyed
from moneyed.localization import _FORMATTER
from decimal import ROUND_HALF_EVEN

MX = moneyed.add_currency(
	code='MX',
	numeric='484',
	name='Peso mexicano',
	countries=('MÉXICO', )
)
_FORMATTER.add_sign_definition(
	'default',
	MX,
)

CURRENCIES = ('MX',)


CART_SESSION_KEY = 'CART'
CART_TEMPLATE_TAG_NAME = 'get_cart'
CART_PRODUCT_MODEL = 'foto.models.Foto'

#paypal
PAYPAL_RECEIVER_EMAIL = "sportr@gmail.com"


SESSION_COOKIE_AGE = 24 * 60 * 60

#recibir informacion
EMAIL_CONTACT = 'victor@dreamsoft.st'

SESSION_COOKIE_AGE = 24 * 60 * 60

EMAIL_HOST = 'smtp.webfaction.com'
EMAIL_HOST_USER = 'dreamsoft'
EMAIL_HOST_PASSWORD = 'saske321.'
DEFAULT_FROM_EMAIL = 'victor@dreamsoft.st'
SERVER_EMAIL = 'victor@dreamsoft.st'
