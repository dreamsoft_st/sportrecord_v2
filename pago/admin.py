from django.contrib import admin
from .models import Compra

@admin.register(Compra)
class CompraAdmin(admin.ModelAdmin):
	list_display = ('usuario', 'Foto', 'fecha', 'pagado',)
	list_display_link = ('usuario', 'Foto', 'fecha', 'pagado',)

	def Foto(self, obj):
		return '<img src="/media/%s" width="100">' % obj.foto.imagen
	Foto.allow_tags = True