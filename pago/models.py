from django.db import models
from foto.models import Foto
from usuario.models import Usuario

class Compra(models.Model):
	foto = models.ForeignKey(Foto)
	usuario = models.ForeignKey(Usuario)
	fecha = models.DateField(auto_now_add=True, null=True)
	pagado = models.BooleanField(default=False)
	tipo_pago = models.CharField(max_length=100, blank=True, editable=False)
	codigo = models.CharField(max_length=100, blank=True, editable=False)

	def __unicode__(self):
		return "%s %s" % (self.foto.nombre, self.usuario.email)