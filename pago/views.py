# -*- coding: utf-8 -*-
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.shortcuts import render, redirect
from carrito.carrito import Cart
from django.conf import settings

from .models import Compra
from foto.models import Foto
from usuario.models import Usuario

from datetime import datetime
from .paypal import Endpoint

from django.core.mail import EmailMultiAlternatives
from django.template.loader import render_to_string
import pdfkit
import os, StringIO
from random import randint
BASE_DIR = os.path.dirname(os.path.dirname(__file__))
from decimal import Decimal

def checkout(request):
	cart = Cart(request.session)
	fotos = cart.products
	return render(request, 'checkout.html', {'fotos': fotos , 'total': cart.total})

@login_required
def checkout_pagar(request):
	cart = Cart(request.session)
	if cart.total <= 0:
		return redirect('/')

	payp = pagar_paypal(request)


	return render(request, 'selectpago.html', {'paypals': payp})


class MyEndPoint(Endpoint):
	def render_to_pdf(self, context_dict):
		html  = render_to_string('email.html', {'pedidos': context_dict, 'BASE': BASE_DIR})
		result = StringIO.StringIO()

		#config = pdfkit.configuration(wkhtmltopdf='/Library/Frameworks/Python.framework/Versions/2.7/lib/python2.7/site-packages/wkhtmltopdf/main.py')
		nombre = 'out_' + str(randint(0, 1000))  + '.pdf'
		salida = os.path.join(BASE_DIR, nombre)
		pdf = pdfkit.from_string(html.encode("ISO-8859-1"), salida)


		subject = '[Aviso] Pago en SportRecord'
		from_email =  'notificaciones@sportrecord.com' 
		to = settings.EMAIL_CONTACT
		msg = EmailMultiAlternatives(subject, '', from_email, [to])

		file_a = open(salida, 'rb')

		msg.attach_alternative(html, "text/html")
		msg.attach("Report.pdf", file_a.read(), "application/pdf")

		msg.send()

		file_a.close()
		try:
			os.remove(salida)
		except OSError:
			pass

	def process(self, data):
		data_o = data
		data = data['custom'].split('}')
		if len(data) > 2:
			maxi = len(data) - 2
			us = Usuario.objects.get(pk=int(data[-1]))
			total = Decimal(data_o['mc_gross'])
			total_pay = Decimal(0)
			if us is not None:
				for x in range(0, maxi):
					f = Foto.objects.get(pk=int(data[x]))
					if f is not None:
						total_pay = Decimal(total_pay) + Decimal(f.precio.amount)
				if total == total_pay:					
					for x in range(0, maxi):
						f = Foto.objects.get(pk=int(data[x]))
						if f is not None:
							Compra.objects.filter(foto=f, usuario=us).update(pagado=True, fecha=datetime.now(), tipo_pago='PAYPAL', codigo=data_o['txn_id'])
							cc = Compra.objects.filter(foto=f, usuario=us)
							self.render_to_pdf(cc)
					cart = Cart( str(data[-2]) )
					cart.clear()
		print 'HECHO'
		
	def process_invalid(self, data):
		print 'ERROR'


def pagar_paypal(request):

	url = request.build_absolute_uri().replace(request.get_full_path(), '')

	paypal_dict = {
		"business": settings.PAYPAL_RECEIVER_EMAIL,
		"currency_code": "MXN",
		"notify_url": url + '/checkout/paypal/',
		"return_url": url + '/mis-fotos/',
		"cancel_return": url + '/checkout/',
		"upload": 1,
		"cmd": "_cart"
	}

	cart = Cart(request.session)
	fotos = cart.products

	x = 1
	fotoss = ''
	for foto in fotos:
		paypal_dict['amount_' + str(x)] = foto.precio
		paypal_dict['item_name_' + str(x)] = foto.slug
		x = x+1
		fotoss = fotoss + str(foto.pk) + '}'
		f = Foto.objects.get(pk=foto.pk)

		obj, created = Compra.objects.get_or_create(foto=f, usuario=request.user)
		obj.fecha = datetime.now()
		obj.save()

	fotoss = fotoss + str(request.session.session_key) + '}' + str(request.user.pk)
	#fotoss = fotoss + str(cart.total) + '&' + str(request.user.pk)

	paypal_dict['custom'] = fotoss

	return paypal_dict