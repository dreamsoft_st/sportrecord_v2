from django.contrib import admin
from .models import Usuario

from django.contrib.auth.admin import UserAdmin

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth import forms as formAdmin


class MyUserCreationForm(UserCreationForm):
	def clean_username(self):
		username = self.cleaned_data["username"]
		try:
			Usuario._default_manager.get(username=username)
		except Usuario.DoesNotExist:
			return username
		raise formAdmin.ValidationError(self.error_messages['duplicate_username'])

	class Meta(UserCreationForm.Meta):
		model = Usuario

@admin.register(Usuario)
class UsuarioAdmin(UserAdmin):
	#model = Usuario
	add_form = MyUserCreationForm

	fieldsets = UserAdmin.fieldsets + (
		(None, {'fields': ('telefono',)}),
	)
