from django.db import models
from django.contrib.auth.models import AbstractUser

class Usuario(AbstractUser):
	telefono = models.CharField(max_length=12, blank=True)
	expire_day = models.DateTimeField(auto_now_add=True)

	def __unicode__(self):
		return self.username