from django.contrib import admin

from .models import Pais, Estado, Ciudad

@admin.register(Pais)
class PaisAdmin(admin.ModelAdmin):
	list_display = ('nombre',)

@admin.register(Estado)
class EstadoAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'pais',)
	raw_id_fields = ('pais',)

@admin.register(Ciudad)
class CiudadAdmin(admin.ModelAdmin):
	list_display = ('nombre', 'estado', 'get_pais')
	raw_id_fields = ('estado',)
	def get_pais(self, obj):
		return obj.estado.pais
	get_pais.short_description = 'Pais'
	get_pais.admin_order_field = 'estado__pais'