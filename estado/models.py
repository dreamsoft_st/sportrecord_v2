from django.db import models

class Pais(models.Model):
	nombre = models.CharField(max_length=100)

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = 'Paises'

class Estado(models.Model):
	nombre = models.CharField(max_length=100)
	pais = models.ForeignKey(Pais)

	def __unicode__(self):
		return self.nombre

class Ciudad(models.Model):
	nombre = models.CharField(max_length=100)
	estado = models.ForeignKey(Estado)

	def __unicode__(self):
		return self.nombre

	class Meta:
		verbose_name_plural = 'Ciudades'