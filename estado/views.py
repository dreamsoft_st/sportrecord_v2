from django.shortcuts import render
from .models import Ciudad
from django.http import JsonResponse, HttpResponse
from django.core import serializers
import json

# Create your views here.
def getciudades(request):
	res = Ciudad.objects.all()
	lista = []
	for c in res:
		lista.append(c.nombre)
	lista = list(set(lista))
	return HttpResponse(json.dumps(lista), content_type="application/json")