from django.http import HttpResponse
from django.contrib.auth.decorators import login_required

from .carrito import Cart
from foto.models import Foto as Product



@login_required
def add(request):
	cart = Cart(request.session)
	product = Product.objects.get(pk=request.POST.get('product_id'))	
	price = product.precio
	if product in cart.products:
		return HttpResponse('La Foto ya se encuentra en el Carrito')
	cart.add(product, price)
	return HttpResponse('Foto Agregada al Carrito')

@login_required
def remove(request):
	cart = Cart(request.session)
	product = Product.objects.get(pk=request.POST.get('product_id'))
	cart.remove(product)
	return HttpResponse('Foto Removida del Carrito')

@login_required
def remove_single(request):
	cart = Cart(request.session)
	product = Product.objects.get(pk=request.POST.get('product_id'))
	cart.remove_single(product)
	return HttpResponse()

@login_required
def clear(request):
	cart = Cart(request.session)
	cart.clear()
	return HttpResponse()

@login_required
def set_quantity(request):
	cart = Cart(request.session)
	product = Product.objects.get(pk=request.POST.get('product_id'))
	quantity = request.POST.get('quantity')
	cart.set_quantity(product, quantity)
	return HttpResponse()