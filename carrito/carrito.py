from decimal import Decimal

from django.conf import settings


from .module_loading import get_product_model as _get_product_model
from carrito import settings as carton_settings


class CartItem(object):
	def __init__(self, product, quantity, price):
		self.product = product
		self.quantity = int(quantity)
		self.price = Decimal(str(price))

	def __repr__(self):
		return u'CartItem Object (%s)' % self.product

	def to_dict(self):
		return {
			'product_pk': self.product.pk,
			'quantity': self.quantity,
			'price': str(self.price),
		}

	@property
	def subtotal(self):
		return self.price * self.quantity


class Cart(object):
	def __init__(self, session, session_key=None):
		self._items_dict = {}
		self.session = session
		self.session_key = session_key or carton_settings.CART_SESSION_KEY

		if self.session_key in self.session:

			cart_representation = self.session[self.session_key]
			ids_in_cart = cart_representation.keys()
			products_queryset = self.get_queryset().filter(pk__in=ids_in_cart)
			for product in products_queryset:
				item = cart_representation[str(product.pk)]
				self._items_dict[product.pk] = CartItem(
					product, item['quantity'], Decimal(item['price'])
				)

	def __contains__(self, product):
		return product in self.products

	def get_product_model(self):
		return _get_product_model()

	def filter_products(self, queryset):
		lookup_parameters = getattr(settings, 'CART_PRODUCT_LOOKUP', None)
		if lookup_parameters:
			queryset = queryset.filter(**lookup_parameters)
		return queryset

	def get_queryset(self):
		product_model = self.get_product_model()
		queryset = product_model._default_manager.all()
		queryset = self.filter_products(queryset)
		return queryset

	def update_session(self):
		self.session[self.session_key] = self.cart_serializable
		self.session.modified = True

	def add(self, product, price=None, quantity=1):
		quantity = int(quantity)
		if quantity < 1:
			raise ValueError('La cantidad debe ser de al menos 1')
		if product in self.products:
			self._items_dict[product.pk].quantity = 1
		else:
			if price == None:
				raise ValueError('Falta agregar el precio')
			self._items_dict[product.pk] = CartItem(product, quantity, price)
		self.update_session()

	def remove(self, product):
		if product in self.products:
			del self._items_dict[product.pk]
			self.update_session()

	def remove_single(self, product):
		if product in self.products:
			if self._items_dict[product.pk].quantity <= 1:
				del self._items_dict[product.pk]
			else:
				self._items_dict[product.pk].quantity -= 1
			self.update_session()

	def clear(self):
		self._items_dict = {}
		self.update_session()

	def set_quantity(self, product, quantity):
		quantity = int(quantity)
		if quantity < 0:
			raise ValueError('La cantidad debe ser positiva')
		if product in self.products:
			self._items_dict[product.pk].quantity = quantity
			if self._items_dict[product.pk].quantity < 1:
				del self._items_dict[product.pk]
			self.update_session()

	@property
	def items(self):
		return self._items_dict.values()

	@property
	def cart_serializable(self):
		cart_representation = {}
		for item in self.items:
			product_id = str(item.product.pk)
			cart_representation[product_id] = item.to_dict()
		return cart_representation


	@property
	def items_serializable(self):
		return self.cart_serializable.items()

	@property
	def count(self):
		return sum([item.quantity for item in self.items])

	@property
	def unique_count(self):
		return len(self._items_dict)

	@property
	def is_empty(self):
		return self.unique_count == 0

	@property
	def products(self):
		return [item.product for item in self.items]

	@property
	def total(self):
		return sum([item.subtotal for item in self.items])