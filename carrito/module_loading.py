from django.conf import settings
from django.utils.importlib import import_module


def get_product_model():
	package, module = settings.CART_PRODUCT_MODEL.rsplit('.', 1)
	return getattr(import_module(package), module)